\chapter{Puesta en marcha y validación}
\label{chap:deploy}
\section{Publicación interna de datos}

En la figura \ref{fig:openapi_full} podemos ver la interfaz gráfica OpenAPI-Swagger de la aplicación de publicación interna de datos con los detalles de cada una de las funciones de la API a las que llama.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{pruebas_y_resultados/openapi_full.png}
    \caption{Interfaz gráfica OpenAPI de la aplicación de publicación interna de datos}\label{fig:openapi_full}
\end{figure}

En la figura \ref{fig:openapi_functiondetail} se observa el detalle de una de las funciones, donde vienen reflejados los parámetros con su nombre, descripción, tipo y su obligatoriedad (o no) de uso. También se muestran los diferentes códigos de respuesta que se pueden recibir.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{pruebas_y_resultados/openapi_functiondetail.png}
    \caption{Detalle de función con parámetros dentro de la interfaz OpenAPI-Swagger}\label{fig:openapi_functiondetail}
\end{figure}

Además de poder utilizar la interfaz de usuario presentada, también se hace constar que esta aplicación se puede invocar mediante peticiones cURL a la API de publicación interna de datos. Gracias a este tipo de peticiones, se puede automatizar la inserción de los ficheros generados como entrada en
el simulador GRAL cuando se programa la ejecución diaria de los modelos de predicción.

En la base de datos encontramos 8345 segmentos de vía en el área de Zaragoza limitada por el rectángulo por defecto, además de 31725 predicciones meteorológicas y 771480 observaciones de flujo de tráfico de 46 estaciones fijas.

En la figura \ref{fig:qgis_shp} se puede ver la visualización con la herramienta QGIS de un ejemplo de los ficheros ESRI Shapefile generados por la aplicación web para la publicación interna de datos que se ha desarrollado dentro de este TFG.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{pruebas_y_resultados/qgis_shp.png}
    \caption{Shapefile generado abierto con QGIS. Mapa de fondo: Carto Dark Matter, Colaboradores de OpenStreetMap.}\label{fig:qgis_shp}
\end{figure}


\section{Publicación de datos abiertos}
\label{sec:opendatadeploy}
\subsection{Descripción del despliegue}

La figura \ref{fig:opendata_comp_traf} muestra el despliegue de los portales de Datos Abiertos en las ciudades de Módena (Italia), Santiago de Compostela (España) y Zaragoza (España). La figura también muestra cómo los servidores GeoServer locales son consultados con el software descrito en los pasos 3 y 4 del flujo de trabajo propuesto para insertar los contenidos de los portales de Datos Abiertos locales. Adicionalmente, la figura muestra los portales de Datos Abiertos a nivel regional, nacional y europeo que recolectan los contenidos de los portales de Datos Abiertos locales.\par

\begin{figure}[ht]
    \centering%
    \fbox{\includegraphics[width=0.85\columnwidth]{pruebas_y_resultados/deployment.pdf}}
    \caption{Despliegue de servidores de Datos Abiertos en las ciudades de Modena, Santiago y Zaragoza}\label{fig:opendata_comp_traf}
\end{figure}

En el caso de Módena, los contenidos de datos abiertos del gobierno municipal de Módena (\textit{Commune di Modena}) se integran directamente en el servidor CKAN mantenido por el gobierno regional de Emilia-Romagna, y posteriormente son recolectados por el portal de Datos Abiertos del Gobierno Italiano (\textit{dati.gov.it}).\par

En el caso de Santiago de Compostela, el portal de Datos Abiertos está gestionado por el gobierno municipal de Santiago de Compostela (\textit{Concello de Santiago}). Después, los contenidos de este portal son recolectados por el portal de Datos Abiertos del Gobierno de España (\textit{datos.gob.es}).\par

El caso de Zaragoza es más complejo. Hay un portal de Datos Abiertos basado en CKAN mantenido por los investigadores de la Universidad de Zaragoza involucrados en el proyecto TRAFAIR. Sin embargo, los contenidos de Datos Abiertos de la universidad son publicados a través de un repositorio institucional: Zaguan, basado en el esquema de metadatos MARC y accesible a través del protocolo OAI-PMH\cite{oaipmh}. En este caso, hemos tenido que implementar un programa específico para cargar periódicamente los metadatos GeoDCAT-AP en formato MARC. Después de ello, estos registros de metadatos son recolectados por el portal del gobierno regional de Aragón (\textit{Aragón Open Data}), y posteriormente por el portal de Datos Abiertos del Gobierno de España.\par

Por último, debe ser notado que los socios de TRAFAIR han decidido desplegar también un portal de Datos Abiertos central que recolecte los contenidos de los portales individuales en cada ciudad. Este portal de Datos Abiertos centra está también basado en la plataforma software CKAN, y sirve como un catálogo unificado para tener una perspectiva global de todos los contenidos de Datos Abiertos a los que han contribuido los distintos colaboradores del proyecto.\par


\subsection{Evaluación de los metadatos}

Para evaluar la calidad de los metadatos generados de forma automática gracias a las herramientas desarrolladas en este TFG y que han sido descritas en el capítulo \ref{chap:opendata}, se ha utilizado la metodología de evaluación de calidad de metadatos propuesta por el Portal Europeo de Datos Abiertos y conocida con el acrónimo MQA (Metadata
Quality Assesment, MQA) \cite{mqa}.\par

Esta metodología es la base del cuadro de mandos utilizado en el contexto del Portal Europeo de Datos para proporcionar una visión general de los contenidos recopilados de los diferentes catálogos que contribuyen a este portal europeo. Inspirado en los principios FAIR\cite{wilkinson2016}, que proporcionan pautas para mejorar la capacidad de búsqueda, accesibilidad, interoperabilidad y reutilización de activos digitales. MQA propone el uso de 23 métricas clasificadas en cinco dimensiones: facilidad de localización, que verifica la disponibilidad de palabras clave, categorías, información espacial e información temporal; accesibilidad, que verifica la accesibilidad de las URLs de acceso y descarga (incluyendo su existencia); interoperabilidad, que verifica el cumplimiento del modelo de metadatos DCAT-AP y la disponibilidad de formatos conocidos (si es posible, no propietarios y legibles por máquina); reusabilidad, que verifica la descripción de la información de licencia y derechos de acceso, así como los puntos de contacto y editores; y contextualidad, que verifica la disponibilidad de información relacionada con los derechos de distribución, el tamaño del archivo de las distribuciones y las fechas de emisión o modificación. Cada una de esas métricas puede ser asignada a un número máximo de puntos de acuerdo al porcentaje de registros de metadatos que verifican la métrica. El número total de puntos para todas las métricas se usa para clasificar catálogos en los rangos de excelente (351-405 puntos), buena (221-350), suficiente (121-220) o mala (0-120) clasificación.\par

\begin{table}[ht]
	\begin{center}
		{\rowcolors{2}{white}{gray!20!white!70}
		\resizebox{0.75\textwidth}{!}{
		\begin{tabular}{ c c c c c c }
			Dimensión & Indicador/propiedad & Válidos & Población & Porcentaje & Puntos \\
			\hline
			Facilidad de localización & dcat:keyword & 19 & 19 & 100 & 30.0 \\
            Facilidad de localización & dcat:theme & 19 & 19 & 100 & 30.0 \\
            Facilidad de localización & dct:spatial & 7 & 19 & 36.84 & 7.368 \\
            Facilidad de localización & dct:temporal & 4 & 19 & 21.05 & 4.21 \\
            Accesibilidad & dcat:accessURL code=200 & 24 & 24 & 100 & 50.0 \\
            Accesibilidad & dcat:downloadURL & 0 & 24 & 0 & 0 \\
            Accesibilidad & dcat:downloadURL code=200 & 0 & 24 & 0 & 0 \\
            Interoperabilidad & dct:format & 24 & 24 & 100 & 20.0 \\
            Interoperabilidad & dcat:mediaType & 0 & 24 & 0 & 0 \\
            Interoperabilidad & dct:format from vocabulary & 0 & 24 & 0 & 0 \\
            Interoperabilidad & dct:format non-proprietary & 5 & 24 & 20.83 & 4.17 \\
            Interoperabilidad & dct:format machine-readable & 0 & 24 & 0 & 0 \\
            Interoperabilidad & DCAT-AP compliance & 0 & 19 & 0 & 0 \\
            Reusabilidad & dct:license & 24 & 24 & 100 & 20.0 \\
            Reusabilidad & dct:license from vocabulary & 0 & 24 & 0 & 0 \\
            Reusabilidad & dct:accessRights & 0 & 19 & 0 & 0 \\
            Reusabilidad & dct:accessRights from vocabulary & 0 & 19 & 0 & 0 \\
            Reusabilidad & dcat:contactPoint & 19 & 19 & 100 & 20.0 \\
            Reusabilidad & dct:publisher & 19 & 19 & 100 & 10.0 \\
            Contextualidad & dct:rights & 24 & 24 & 100 & 5.0 \\
            Contextualidad & dcat:byteSize & 0 & 24 & 0 & 0 \\
            Contextualidad & dct:issued & 19 & 19 & 100 & 5.0 \\
            Contextualidad & dct:modified & 19 & 19 & 100 & 5.0 \\
            \multicolumn{6}{c}{Puntos totales 210.75} \\
            \rowcolor{white}\multicolumn{6}{c}{Clasificación: \textbf{Suficiente} (rango 121-220)}
		\end{tabular}}}
	\end{center}
	\caption{Salida de la herramienta MQA para los conjuntos de datos en CKAN el 20 de mayo de 2020}
	\label{tab:mqa}
\end{table}

Como se puede comprobar, la clasificación es suficiente alta. Los campos con porcentaje 0 se deben principalmente a dos motivos: por un lado, algunos campos evaluados por MQA no se consideran en GeoDCAT-AP; por otro lado, existen algunos campos para cuyos valores se chequea la pertenencia a un vocabulario, donde los vocabularios propuestos por MQA están pensados para recursos de dominios más generales, no específicos del mundo de información geográfica.\par
Para el resto de campos se comprueba que los metadatos generados cumplen con la metodología de calidad.
