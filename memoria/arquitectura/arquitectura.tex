\chapter{Visión general}
\label{chap:general}

En la figura \ref{fig:trafair} se puede observar la arquitectura general del proyecto TRAFAIR. Esta se compone de cuatro fases diferenciadas.\par
Primero se encuentra la fase de Entrada, que recoge información relevante para el proyecto de diversas fuentes de datos, propias y externas. Estos datos se transforman y se introducen en la base de datos del proyecto para su uso en fases posteriores.\par
En segundo lugar, tenemos la fase de Modelado/Simulación, donde se aúnan las entradas en modelos que ayudan a entender el problema planteado en el proyecto. Como objetivo de esta fase se desea llegar al modelo lagrangiano GRAL\cite{gral} y realizar con esos datos la simulación del estado de la calidad del aire en las ciudades del proyecto día a día.\par
Esta simulación genera una salida que se guarda en la base de datos y posteriormente en la última fase, la de Publicación de Datos Abiertos, se libera al público, junto con los datos de entrada que son recogidos directamente por el proyecto (mediciones de sensores de bajo coste).\par

En la figura \ref{fig:components} se muestra una arquitectura en capas donde se puede ver los principales almacenes de datos, los metadatos que los describen y los componentes software que facilitan el acceso y publicación de los mismos desde el exterior. 
Además, en esta figura se puede ver que el proyecto pretende ser accesible desde dos perspectivas distintas: desde el punto de vista de las Infraestructuras de Datos Espaciales (IDE), y desde el mundo de los portales de Datos Abiertos.
Respecto a la publicación interna, se puede ver cómo el componente Web App permite acceder a los datos necesarios por GRAL. Por otro lado, la publicación externa se consigue a través de portales CKAN que facilitan el acceso a los conjuntos de datos abiertos y que enlazan con los contenidos reales a través de servicios del mundo IDE conformes con las especificaciones establecidas por el Open Geospatial Consortium (OGC): Web Mapping Services (WMS) para la visualización, Web Feature Services (WFS) para la descarga de datos de fenómenos discretos (objetos reconocibles que tienen un contorno o una extensión espacial bien definida a los que se asocian \\
\begin{figure}[H]
	\centering
		\begin{sideways}
			\begin{minipage}{17.5cm}
				\input{arquitectura/trafair_general}
			\end{minipage}
		\end{sideways}
	\caption{Flujo de datos del proyecto TRAFAIR dentro de la ciudad de Zaragoza}\label{fig:trafair}
\end{figure}\break
\noindent atributos temáticos, por ejemplo, edificios con altura, tipo de habitabilidad, fecha de construcción, ...), y Web Coverage Services (WCS) para la descarga de datos de coberturas (fenómenos continuos que varían sobre el espacio y sin una extensión específica, por ejemplo, una cobertura con la temperatura sobre la superficie del mar). 

\begin{figure}[ht]
\centering%
\includegraphics[width=0.75\textwidth]{arquitectura/tierArchitecture.pdf}
\caption{Arquitectura de componentes de datos y servicios en el proyecto TRAFAIR}\label{fig:components}
\end{figure}

\section{Publicación interna de datos}

La aplicación de publicación interna de datos se enmarca dentro de la fase Modelado/Simulación del \hyperref[fig:trafair]{flujo de datos del proyecto (figura \ref{fig:trafair})}. Su función es generar archivos intermedios dentro de la fase para la simulación y visualización a partir de los datos introducidos en la base de datos del proyecto.\par

En la figura \ref{fig:gral} se muestra el flujo de datos implicado en el modelado/simulación y los conjuntos de datos que es necesario proporcionar al modelo GRAL \cite{marrodan2019}. En primer lugar se genera un modelo de tráfico con el simulador SUMO, calculando el flujo estimado para cada calle de la ciudad. Posteriormente se calcula el modelo de emisiones con VEIN, que calcula las emisiones NOx que produce el modelo de tráfico del paso anterior, y este valor de contaminante se guarda en la base de datos asociado a cada calle. Este trabajo de simulación se desarrolla en el TFG de David Sáez \cite{TFGSaez}.

La aplicación de publicación interna de datos genera entonces los archivos necesarios como entrada a GRAL a partir de la base de datos, tanto de red viaria con emisiones NOx, como de factores de emisión y predicción meteorológica. Se explica detalladamente cada función en el capítulo \ref{chap:internal}.

\begin{figure}[ht]
\centering%
\includegraphics[width=0.75\textwidth]{arquitectura/GRAL.pdf}
\caption{Datos de entrada necesarios para la ejecución del modelo de predicción GRAL}\label{fig:gral}
\end{figure}

\section{Publicación de datos abiertos}
La aplicación de publicación de datos abiertos se localiza en la fase Publicación de Datos del \hyperref[fig:trafair]{diagrama de flujo de datos del proyecto (figura \ref{fig:trafair})}.\par

La publicación de servicios OGC únicamente no se puede considerar publicación de Datos Abiertos. Para conseguir que los datos sean realmente accesibles como Datos Abiertos, necesitamos registrar los conjuntos de datos en los portales de Datos Abiertos oficiales. Además, la publicación de conjuntos de datos en el Portal Europeo de Datos\cite{edp} es un requerimiento del proyecto.\par

Con el objetivo de registrar los datos de TRAFAIR como datos abiertos, el primer paso ha sido seleccionar un perfil de metadatos apropiado conforme con los modelos de metadatos aceptados en el contexto del Dominio Abierto. Teniendo en cuenta el carácter espacial de los datos generados por TRAFAIR, se ha adaptado el perfil de metadatos GeoDCAT-AP\cite{geodcatap}. GeoDCAT-AP es un perfil de metadatos que extiende DCAT-AP\cite{dcatap}\cite{dcat}, un perfil de metadatos diseñado por la Comisión Europea para describir datos del sector público. Las propiedades de metadatos GeoDCAT-AP han sido diseñadas para asegurar la conformidad con los requerimientos de metadatos de la directiva europea INSPIRE para establecer una infraestructura de información espacial en Europa \cite{inspire}.\par

Por otro lado, para minimizar el esfuerzo de crear metadatos y el registro de esos datos en portales de Datos Abiertos, hemos decidido automatizar este proceso por medio de un software que recupera las capacidades de los servicios OGC y convierte esta información a registros de metadatos que posteriormente son insertados en un servidor de Datos Abiertos CKAN.
CKAN\cite{ckan} es la plataforma de código abierto más ampliamente usada para mantener portales de Datos Abiertos, que incluye los complementos necesarios para intercambiar metadatos en formato RDF (el formato de serialización utilizado para GeoDCAT-AP)\cite{ckanext-dcat}.\par
Se valoró también como herramienta para gestionar un portal de datos abiertos nos encontramos con dos principales el uso de DKAN\cite{dkan}.\par
Ambos son muy similares, con la diferencia de CKAN estar programado en Python y DKAN en PHP (Drupal). Dado que CKAN tiene una comunidad mayor, y más documentación e integración con otras herramientas se escogió como software para este proyecto.\par
Diversos complementos se han usado para completar los requerimientos del proyecto, listados en el anexo \ref{app:ckan}, donde se explica la instalación en detalle de CKAN junto con los complementos que se han utilizado.
También, al desear que los datos sean accesibles dentro de una IDE, se pone en marcha un servicio CSW\cite{csw} con la conexión de PyCSW\cite{pycsw} con CKAN via ckanext-spatial\cite{ckanext-spatial}.