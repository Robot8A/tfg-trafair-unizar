\chapter{Introducción}
\section{Contexto y motivación}

Este Trabajo de Fin de Grado (TFG) se enmarca dentro del proyecto europeo TRAFAIR (\textit{Understanding traffic flows to improve air quality})\cite{trafair}, financiado por el programa Connecting Europe Facility de la Unión Europea (Proyecto Nº 2017-EU-IA-0167). El consorcio que desarrolla este proyecto reúne a 10 organizaciones de Italia, Bélgica y España, entre ellas la Universidad de Zaragoza, para desarrollar servicios innovadores empleando datos de calidad del aire, condiciones meteorológicas y flujos de tráfico, en beneficio tanto de los ciudadanos, como de las autoridades responsables de la toma de decisiones que afectan a la calidad del aire.

Los principales objetivos del proyecto son la monitorización en tiempo real de la contaminación del aire en zonas urbanas, y el desarrollo de un servicio de predicción de la calidad del aire basado en predicciones meteorológicas y del flujo de tráfico urbano. Los resultados del proyecto se validan en seis ciudades piloto (Módena, Florencia, Pisa, Livorno, Santiago de Compostela y Zaragoza) y en cada una de ellas se pretende desarrollar un modelo del tráfico rodado para que, junto con la predicción de las condiciones meteorológicas, principalmente datos de la dirección y velocidad del viento, se analice la dispersión de los contaminantes dentro de la ciudad. Por otro lado, también se instalan una serie de sensores electroquímicos de bajo coste para medir niveles de contaminación repartidos en distintos puntos de cada ciudad. Además, se pretende publicar los datos de monitorización y predicción de calidad del aire como datos abiertos y crear diferentes aplicaciones móviles para informar a los ciudadanos de la calidad del aire que respiramos y tratar de concienciar en un uso responsable del automóvil privado.  En la figura \ref{fig:objectives} se resumen los principales datos de entrada y objetivos del proyecto \cite{viqueira2019}.



\begin{figure}[ht]
\centering%
\includegraphics[width=0.75\textwidth]{introducción/objectives.pdf}
\caption{Principales datos de entrada y objetivos de TRAFAIR}\label{fig:objectives}
\end{figure}

En este TFG se pretende contribuir a los desarrollos implicados en los procesos necesarios para la publicación interna y externa de datos. Por publicación interna nos referimos al procesamiento y generación de los datos que son necesarios como entrada para los modelos de predicción. Por publicación externa nos referimos al procesamiento que es necesario realizar para incluir los conjuntos de datos de monitorización y predicción que se generan en TRAFAIR dentro de iniciativas de portales de datos abiertos.

Como datos abiertos nos referimos a aquellos que tienen una licencia que permite su reutilización sin ningún permiso específico, y son accesibles para el público general\cite{opendata}.


\section{Objetivos}
El objetivo de este TFG es proporcionar los mecanismos de publicación de datos requeridos tanto internamente para acceder a los datos necesarios para la ejecución de los modelos de predicción, como externamente para la difusión de los resultados de predicción y monitorización. Dado el caracter geográfico de los datos utilizados y generados en el proyecto TRAFAIR, para el modelado, almacenamiento y gestión de los datos se utilizan las herramientas habituales de los Sistemas de Información Geográfica (SIG) y las Infraestructuras de Datos Espaciales (IDE)\footnote{Una IDE se define como un conjunto de tecnologías, políticas y disposiciones institucionales que facilitan el acceso y explotación de datos espaciales\cite{nebert2004}.}.\par

Respecto a los mecanismos de publicación, este TFG cuenta con dos secciones claramente diferenciadas: la publicación interna de datos, donde se transforman diversos conjuntos de datos de la base de datos en fases tempranas del proyecto y se vuelcan por una API para su uso por parte de los investigadores, tanto para corroborar la corrección de los datos como para servirlos de entrada a la simulación en las fases medias del proyecto; y la publicación de datos abiertos, donde se seleccionan los conjuntos de datos con relevancia para el público general y se ponen a la disposición de la ciudadanía en diversos formatos y siguiendo los estándares descritos en este documento.\par

También se pretende que los datos abiertos sean interoperables entre administraciones usando los estándares y normas internacionales. En primer lugar, el proyecto persigue que los conjuntos de datos sean accessibles a través de los servicios habituales de localización o catálogos de las IDE. Es decir, queremos que los metadatos que describen a los conjuntos de datos geográficos generados en TRAFAIR sean localizables a través de servicios de catálogo de una IDE que a su vez son unos servicios conformes a la especificación Catalog Services for the Web (CSW)\cite{csw} propuesta por el consorcio Open Geospatial Consortium (OGC).\par

Pero como los servicios de catálogo de una IDE no son fácilmente accesibles desde dominios más generales, también se pretende integrar los metadatos que describen los conjuntos de datos (y los servicios de visualización y descarga) de TRAFAIR dentro de las iniciativas de portales de datos abiertos, liberándose a toda la ciudadanía mediante su publicación en un portal de datos abiertos propio del proyecto además de estar incluidos los diferentes conjuntos en los diversos portales de las administraciones: desde portales locales, a portales nacionales, o el portal de datos Europeo.


\section{Tecnologías utilizadas}
Se ha decidido utilizar Python3\cite{python} como lenguaje principal del proyecto ya que es el lenguaje con el que el autor del proyecto se siente más cómodo. Al tener una baja carga de trabajo las pasarelas que se han tenido que programar, ser la temporización del proyecto no crítica y ser la comunicación con los diferentes servicios por medio de APIs y peticiones HTTP, se puede utilizar una gran variedad de lenguajes sin influir en el resultado final. Se ha seguido la guía de estilo PEP8\cite{pep8} de normalización de Python.

Para facilitar el acceso a los datos del proyecto a través de de las especificaciones estándar habituales en el mundo de las IDE y propuestas por la organización Open Geospatial Consortium (OGC) se ha utilizado el software Geoserver \cite{geoserver}. Este software permite poner en marcha un servidor conectado con la base de datos espacial PostGIS del proyecto y ofrecer, entre otros, los siguientes servicios compatibles con las especificaciones de OGC: servicio de visualización WMS (Web Mapping Service)\cite{wms}, servicio de descarga de fenómenos discretos
WFS (Web Feature Service)\cite{wfs} y servicio de descarga de coberturas (WCS)\cite{wcs}.

Como herramienta de distribución de datos abiertos en un portal se ha utilizado el software CKAN\cite{ckan}. Se ha elegido por su facilidad de uso y su extensa documentación y extensiones para facilitar cumplir los objetivos del proyecto, además de ser también la herramienta utilizada en numerosos portales de referencia (Gobierno de Aragón, Gobierno de España, Comisión Europea).\par
Además de los servicios OGC brindados por GeoServer, se ha utilizado también el software PyCSW\cite{pycsw}. Este software permite poner en marcha un servidor de catálogo de metadatos geográficos compatible con la especificación de OGC para servicios de catálogo denominada Catalog Services for the Web (CSW)\cite{csw}. Este software tiene la ventaja de integrarse fácilmente con los servidores CKAN gracias a la extensión ckanext-spatial\cite{ckanext-spatial}.\par

\section{Estructura de la memoria}
El resto de la memoria se compone de los siguientes capítulos:
\begin{itemize}
    \item El Capítulo \ref{chap:general} proporciona una visión general del proyecto, incluyendo diagramas de arquitectura, y realiza breves anotaciones sobre el diseño.
    \item El Capítulo \ref{chap:internal} expone la primera sección del trabajo: la publicación interna de datos.\par
    \item El Capítulo \ref{chap:opendata} explica la segunda sección del trabajo: la publicación de Datos Abiertos.\par
    \item El Capítulo \ref{chap:deploy} muestra la puesta en marcha y validación de ambas secciones.\par
    \item El Capítulo \ref{chap:management} explica cómo se ha llevado la gestión del proyecto y desarrolla las conclusiones sobre el trabajo y trabajo futuro a realizar.\par
    \item Se incluyen además dos anexos.
        \begin{itemize}
            \item El Anexo \ref{app:ckan} detalla la instalación de CKAN con sus complementos.
            \item Y en el Anexo \ref{app:dataset} se enseña el ejemplo de un conjunto de datos en sus diferentes fases dentro de la Publicación de Datos Abiertos.
        \end{itemize}
\end{itemize}