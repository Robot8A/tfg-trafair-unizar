\chapter{Publicación de datos abiertos}
\label{chap:opendata}
En la figura \ref{fig:opendata_comp} podemos ver los componentes que influyen en la publicación de datos abiertos del proyecto. Se puede interpretar también como un diagrama de flujo de abajo a arriba, siendo GeoServer el componente que se conecta directamente con la base de datos del proyecto y sirve los datos mediante servicios de visualización y descarga compatibles con las especificaciones de OGC, y subiendo de forma automatizada a los servicios de ámbito superior para alcanzar cada vez a una mayor parte de la ciudadanía. El paso intermedio por la pasarela CKAN-Zaguan y Zaguan se realiza solo en Zaragoza, en los demás casos el servidor CKAN de nivel superior recolecta directamente del local, se explican los casos concretos en la sección \ref{sec:opendatadeploy}.

\begin{figure}[htb]
	\begin{center}
    	\resizebox{0.75\textwidth}{!}{
    		\begin{tikzpicture}
    			\umlbasiccomponent[y=0]{GeoServer}
    			\umlbasiccomponent[y=3,fill= 	YellowOrange]{Pasarela ogc-ckan}
    			\umlbasiccomponent[y=6]{Servidor CKAN local}
    			\umlbasiccomponent[x=11,y=6.5]{PyCSW}
    			\umlbasiccomponent[y=9,fill= 	YellowOrange]{Pasarela ckan-zaguan}
    			\umlbasiccomponent[y=12]{Zaguan}
    			\umlbasiccomponent[y=15]{Servidor CKAN de nivel superior}
    			\umlHVHassemblyconnector[interface=WMS/WFS/WCS GetCapabilities,anchor1=east,anchor2=east,arm2=4]{Pasarela ogc-ckan}{GeoServer}
    			\umlHVHassemblyconnector[interface=CKAN Upload API,anchor1=west,anchor2=west,arm1=-4]{Pasarela ogc-ckan}{Servidor CKAN local}
    			\umlHVHassemblyconnector[interface=GeoDCAT-AP RDF,anchor1=10,anchor2=20,arm2=2]{Pasarela ckan-zaguan}{Servidor CKAN local}
    			\umlVHassemblyconnector[interface=,anchor1=south,anchor2=340,arm2=4]{PyCSW}{Servidor CKAN local}
    			\umlprovidedinterface[interface=CSW,distance=2cm,anchor=west]{PyCSW} 
    			\umlHVHassemblyconnector[interface=MARC21 REST API,anchor1=west,anchor2=west,arm1=-4]{Pasarela ckan-zaguan}{Zaguan}
    			\umlHVHassemblyconnector[interface=GeoDCAT-AP RDF,anchor1=east,anchor2=east,arm2=5]{Servidor CKAN de nivel superior}{Servidor CKAN local}
    			\umlHVHassemblyconnector[interface=OAI-PMH,anchor1=350,anchor2=east,arm2=3]{Servidor CKAN de nivel superior}{Zaguan}
    		\end{tikzpicture}
		}
	\caption{Diagrama de componentes de publicación de datos abiertos}\label{fig:opendata_comp}
	\end{center}
\end{figure}

En la sección \ref{sec:metadataprofile} se describe de forma resumida el perfil de metadatos que se ha utilizado.\par
En la sección \ref{sec:ogcckan} se describe el flujo de trabajo para publicar en un portal de tipo CKAN la información recolectada a través de los servicios OGC.\par
En la sección \ref{sec:ckanzaguan} se describe el flujo de trabajo para publicar en el repositorio institucional de documentos Zaguan. Este paso sólo es realizado en el caso de Zaragoza.\par
Por último, en la sección \ref{sec:pycsw} se describen los pasos realizados para publicar en un servicio CSW.


\section{Perfil de metadatos basado en GeoDCAT-AP}
\label{sec:metadataprofile}

ISO 19115 es el estándar internacional para metadatos geográficos propuesto por la Organización Internacional de Normalización (ISO por sus siglas en inglés)\cite{iso19115}, que ha sido ampliamente adoptado durante la última década en la comunidad de de la información geográfica en sectores tanto públicos como privados.\par

Sin embargo, en el dominio de datos abiertos se necesitan esquemas de metadatos más generales y simples para facilitar la publicación de conjuntos de datos de diferentes disciplinas en el mismo repositorio de metadatos. DCAT es el acrónimo del vocabulario del Catálogo de datos del W3C\cite{dcat} y puede ser considerado como un núcleo básico y general de propiedades de metadatos compartidas por los diferentes esquemas de metadatos usados en diversas iniciativas de Datos Abiertos. En el caso europeo, la Unión Europea propuso en 2013 DCAT-AP \cite{dcatap}, una especificación basada en DCAT para describir conjuntos de datos del sector público en Europa. Comparado con DCAT, DCAT-AP provee definiciones más estrictas de catálogos, conjuntos de datos, distribuciones y otros objetos.\par

Como se ha mencionado en la introducción, en el contexto de este proyecto hemos seleccionado GeoDCAT-AP v1.01 \cite{geodcatap}. Esta extensión de DCAT-AP está diseñada para la descripción de datos espaciales y sus propiedades de metadatos tienen un mapeo exacto con los elementos principales de los metadatos ISO 19115. Este mapeo garantiza la transformación de registros GeoDCAT-AP a su equivalente en ISO 19115 conforme con los requerimientos INSPIRE\cite{inspire}\cite{inspireTGMetadata2017}.\par

%Únicamente para el caso de España, la administración pública establece un Esquema Nacional de Interoperabilidad, donde se enmarca la NTI (\enquote{Norma Técnica de Interoperabilidad de Reutilización de Recursos de Información})\cite{boe_nti}\cite{boe_nti2}

\begin{figure}[ht]
\centering%
\includegraphics[width=0.75\columnwidth]{publicación_datos_abiertos/geodcat.pdf}
\caption{Entidades y propiedades usadas de GeoDCAT-AP}\label{fig:metadataProfile}
\end{figure}

La descripción de conjuntos de datos según GeoDCAT-AP está principalmente pensada en proveer información sobre tres entidades principales: un \textit{Catálogo} (Catalog) que se publica mediante un portal de Datos Abiertos conteniendo \textit{Conjuntos de datos} (Datasets) y \textit{Distribuciones} (Distributions), las formas asociadas para cada conjunto de datos. Además, GeoDCAT-AP hace una distinción entre propiedades principales (core) y extendidas (extended). El conjunto principal es la selección de propiedades de metadatos DCAT-AP que tienen una conexión directa con los metadatos ISO 19115 e INSPIRE. En algunos casos, estas propiedades adicionales pertenecen a otros vocabularios de metadatos. En otros casos, aunque las propiedades pertenezcan a DCAT-AP, son clasificadas como extendidas al proveer tan solo una conexión o mapeo parcial con ISO 19115 e INSPIRE.\par

La figura \ref{fig:metadataProfile} muestra un diagrama UML con las propiedades de GeoDCAT-AP que son necesarias para describir conjuntos de datos y distribuciones en TRAFAIR. La mayoría de estas propiedades pertenecen al conjunto principal de GeoDCAT-AP. Las únicas excepciones son las propiedades \textit{dct:type} de \textit{Conjuntos de datos} (Datasets) y la propiedad \textit{dct:description} de \textit{Distribuciones} (Distributions). \textit{dct:type} es empleado para indicar si el recurso descrito es un conjunto de datos o una serie de conjuntos de datos. \textit{dct:description} permite la descripción de la resolución espacial de las distribuciones asociadas.\footnote{A pesar de que GeoDCAT-AP propone \textit{rdfs:comment} como una propiedad provisional para rellenar esta información de resolución, no hay un mapeo directo de esta propiedad a campos de CKAN y por lo tanto hemos considerado \textit{dct:description} como una alternativa válida.}



\section{Servicio de publicación automática de metadatos en servidores CKAN a partir de servicios OGC}
\label{sec:ogcckan}

La figura \ref{fig:flowchart} muestra un diagrama de actividad con los cinco pasos principales del flujo de trabajo que hemos propuesto para la publicación de Datos Abiertos Espaciales. Para los pasos 1, 3 y 4 hemos desarrollado software para automatizar lo máximo posible la generación y publicación de metadatos. Los pasos 2 y 5 son conseguidos gracias al uso de programas ya existentes.

\begin{figure}[ht]
\centering
\resizebox{0.75\textwidth}{!}{
\begin{tikzpicture}
% basicstate and basicrect defined in tikzstyles/tikzstyles.tex
\umlstateinitial[x=3,y=0,name=begin]
\node[basicstate] at (3,-2) (step1){1. Ingestión de capas en el \mbox{almacén} de datos espaciales};% Use mbox to prevent hyphenation in undesired places
\node[basicstate] at (3,-4) (step2){2. Publicación de \mbox{capas} como servicios OGC};
\node[basicstate] at (3,-6) (step3){3. Recolector de metadatos OGC};
\node[basicstate] at (3,-8) (step4){4. Ingestión de metadatos en servidor local de Datos Abiertos};
\node[basicstate] at (3,-10) (step5){5. Recolección de metadatos por portales de Datos abiertos intermedios y el Portal Europeo de Datos};
\umlstatefinal[x=3,y=-12,name=end]
\node[basicrect] at (-3,-1) (ftc){Capas de tipo Fenóm. discretos o Cobertura};
\node[basicrect] at (-3,-3) (usw){Almacén de datos espaciales actualizado};
\node[basicrect] at (-3,-5) (ogc){OGC \mbox{WMS/WFS/WCS}};
\node[basicrect] at (-3,-7) (gda){Metadatos GeoDCAT-AP};
\node[basicrect] at (-3,-9) (uls){Servidor local de Datos Abiertos actualizado};
\node[basicrect] at (-3,-11.5) (uus){Servidores de Datos Abiertos de nivel superior actualizados};
\umltrans{begin}{step1}
\umltrans{ftc}{step1}
\umltrans{step1}{step2}
\umltrans{step1}{usw}
\umltrans{usw}{step2}
\umltrans{step2}{step3}
\umltrans{step2}{ogc}
\umltrans{ogc}{step3}
\umltrans{step3}{step4}
\umltrans{step3}{gda}
\umltrans{gda}{step4}
\umltrans{step4}{step5}
\umltrans{step4}{uls}
\umltrans{uls}{step5}
\umltrans{step5}{end}
\umltrans{step5}{uus}
\end{tikzpicture}
}
\caption{Flujo de trabajo para la publicación de Datos Abiertos Espaciales}\label{fig:flowchart}
\end{figure}

El primer paso es la ingestión de capas en un almacén de datos espaciales. Geoserver\cite{geoserver} es el software elegido para gestionar la publicación de capas de datos espaciales, tanto para fenómenos discretos (en inglés: Feature Type) como para datos de cobertura (Coverage), ya que permite una fácil conexión con la base de datos, permitiendo seleccionar las tablas que se necesiten o una consulta como capas de GeoServer. También posibilita la creación de metadatos y estilos personalizados para cada capa para cumplir las necesidades del proyecto y facilita la introducción de atributos comunes como la información de contacto del administrador del servidor.\par
En el contexto del proyecto TRAFAIR se ha desarrollado software específico en los lenguajes Java y R\footnote{Este software no ha sido desarrollado por el autor del trabajo, sino por otros compañeros del proyecto y por lo tanto no entra dentro de este TFG.} para la ingestión de capas de datos discretos (soportadas en una base de datos espacial) y coberturas en GeoServer a través de su API REST\cite{featuresAPI}\cite{coverageAPI}. Con respecto a la generación de metadatos, este software se encarga de enviar a la API REST los valores apropiados para las etiquetas enumeradas en la columna \textit{Geoserver} de la tabla \ref{tab:correspondence}.\par

El segundo paso es la publicación de capas como servicios OGC. Este paso es conseguido directamente gracias al software de GeoServer, que provee acceso a capas a través de diferentes servicios OGC. Capas de fenómenos discretos como observaciones del tráfico y de sensores de calidad del aire pueden ser descargadas desde un servicio WFS. En el caso de coberturas para monitorización de la calidad del aire (interpolaciones de observaciones de sensores) o coberturas para la predicción de calidad (el resultado de un modelo GRAL), un servicio WCS es brindado. Más allá de WFS y WCS, algunas capas también están disponibles para servir representaciones de mapas generadas del lado del servidor utilizando un servicio WMS.\par

El tercer paso es la recolección de metadatos de los servicios OGC a través de su operación \textit{GetCapabilities}. Para implementar este paso, hemos desarrollado un programa en Python que aprovecha la librería OWSLib\cite{owslib}, un paquete de cliente para interactuar con las interfaces de los servicios OGC y sus modelos de contenido relacionados. Este software interacciona con la interfaz OGC, en vez de la API REST de GeoServer, ya que queríamos hacer este software lo suficientemente escalable para integrar en un futuro otras capas gestionadas por otros paquetes software distintos de GeoServer. OWSLib transforma los modelos de los servicios a clases de Python, pudiendo directamente trabajar de forma cómoda con objetos de dichas clases y recopilar los atributos necesarios para nuestra pasarela.\par

\begin{table}[H]
\centering
\resizebox{\textwidth}{!}{
\begin{scriptsize}
\begin{tabular}{|p{1.7in}|p{2.3in}|p{1.3in}|l|}
\hline
\textbf{Geoserver}&\textbf{OWSLib}&\textbf{CKAN}&\textbf{GeoDCAT-AP}\\
\hline
featureType/name, \mbox{coverage/name}&\textit{layerName}&extra:identifier&Dataset/dct:identifier\\
\hline
featureType/title, coverage/title&contents[\textit{layerName}].title&title&Dataset/dct:title\\
\hline
featureType/description, \mbox{coverage/description} \textit{(software en el paso 1 introduce descripciones predeterminadas de acuerdo a patrones de nombres)}&contents[\textit{layerName}].abstract&notes&Dataset/dct:description\\
\hline
&\textit{(''series'' para servicios OGC con dimensión temporal o ``dataset'' sin dimensión temporal)}&extra:dcat\_type&Dataset/dct:type\\
\hline
&\textit{(idioma por defecto propuesto en el paso 3)}&extra:language&Dataset/dct:language\\
\hline
&\textit{(temas de datos por defecto de INSPIRE y categorías de temas ISO 19115 propuestos en el paso 3)} &extra:theme&Dataset/dcat:theme\\
\hline
\textit{(algunas palabras clave son introducidas automáticamente por Geoserver)}&contents[\textit{layerName}].keywords&tags&Dataset/dcat:keyword\\
\hline
\textit{(computado automáticamente por Geoserver)}&contents[\textit{layerName}].boundingBoxWGS84&extra:spatial&Dataset/dct:spatial\\
\hline
\textit{(fecha de inicio y fecha final son actualizadas automáticamente por Geoserver)}&contents[\textit{layerName}].timepositions&extra:temporal\_start + extra:temporal\_end&Dataset/dct:temporal\\
\hline
&&extra:issued \textit{(insertado automáticamente con la primera ingestión en CKAN)} &Dataset/dct:issued\\
\hline
&&extra:modified \textit{(actualizado automáticamente con cada actualización de conjunto de datos en CKAN)} &Dataset/dct:modified\\
\hline
&\textit{(provenencia por defecto propuesta en el paso 3)}&extra:provenance&Dataset/dct:provenance\\
\hline
&\textit{(conformidad INSPIRE por defecto y sistema de referencia de coordenadas propuesto en el paso 3)}&extra:conforms\_to&Dataset/dct:conformsTo\\
\hline
\multirow{2}{*}{\parbox{\linewidth}{\textit{(la información de contacto es introducida directamente por los administradores en la página de configuración de GeoServer)}}}&contents[\textit{layerName}].provider.contact. organization&extra:publisher\_name&Dataset/dct:publisher\\
\cline{2-4}
&contents[\textit{layerName}].provider.contact.name + contents[\textit{layerName}].provider.contact.email&extra:contact\_name + extra:contact\_email&Dataset/dcat:contactPoint\\
\hline
\textit{(URL de servicio OGC generada automáticamente por Geoserver)}&\textit{(OGC service URL)}&resource:url&Distribution/dcat:accessURL\\
\hline
featureType/name, \mbox{coverage/name}&\textit{layerName}&resource:name&Distribution/dct:title\\
\hline
featureType/serviceConfiguration, coverage/serviceConfiguration&\textit{(``wfs'', ``wcs'' o ``wms'' según tipo de servicio OGC)}&resource:format&Distribution/dct:format\\
\hline
&\textit{(licencia por defecto propuesta en paso 3)}&resource:license&Distribution/dct:license\\
\hline
&\textit{(derechos por defecto propuestos en paso 3)}&resource:rights&Distribution/dct:rights\\
\hline
&\textit{(resolución por defecto propuesta para los conjuntos de datos del proyecto)}&resource:description&Distribution/dcat:description\\
\hline
\end{tabular}
\end{scriptsize}
}
\caption{Correspondencia entre etiquetas de GeoServer (contenidas en el cuerpo de una petición POST para crear una capa de fenómenos discretos o cobertura), campos de la capa recuperado con OWSLib de una respuesta GetCapabilities, etiquetas de CKAN (contenidas en el cuerpo de una petición POST para crear un conjunto de datos), y propiedades GeoDCAT-AP.}
\label{tab:correspondence}
\end{table}

En la figura \ref{fig:geockanbridge_classes} se pueden observar las clases de este programa Python, que hemos denominado Pasarela OGC-CKAN.\par

El algoritmo detrás de este software genera una instancia de \textit{Dataset} (conjunto de datos) por cada capa publicada en los servicios WFS o WCS. Además, cada \textit{Dataset} tiene al menos una instancia de \textit{Distribution} (distribución) asociada en la forma de un enlace a un servicio WFS o WCS.\par

En algunos casos, si la capa es también renderizada a través de un WMS, una segunda distribución enlazando al servicio WMS es generada. La columna \textit{OWSLib} en la tabla \ref{tab:correspondence} muestra los campos recuperados por el paquete OWSLib para generar la propiedad GeoDCAT-AP correspondiente.\par

\begin{figure}[H]
	\begin{center}
    	\resizebox{\textwidth}{!}{
    		\begin{tikzpicture}
    			\begin{umlpackage}{ogc ckan}
    			    \umlclass[x=0,y=16]{Distribution}{
    			        url: str \\ name: str \\ format: str \\ license: str \\ rights: str \\ description: str
    			    }{
    			        \_\_init\_\_(...) \\ set\_url(self, url) \\ set\_name(self, name) \\ set\_format(self, format) \\ set\_license(self, license) \\ set\_rights(self, rights) \\ set\_description(self, description) \\ def to\_dict(self)
    			    }
    			    \umlclass[x=0,y=0]{Dataset}{
    			        name: str \\ publisher: str \\ owner\_org: str \\ identifier: str \\ title: str \\ notes: str \\ resourceType: str \\ languages: str \\ themes: str \\ keywords: array(str) \\ spatial: str \\ temporalStart: str \\ temporalEnd: str \\ issued: str \\ modified: str \\ provenance: str \\ conformance: str \\ contactName: str \\ contactEmail: str \\ distributions: array(str)
    			    }{
    			        \_\_init\_\_(...) \\ set\_name(self, name) \\ set\_publisher(self, publisher) \\ set\_identifier(self, identifier) \\ set\_title(self, title) \\ set\_description(self, notes) \\ set\_resource\_type(self, resource\_type) \\ set\_languages(self, languages) \\ set\_themes(self, themes) \\ set\_keywords(self, keywords)\\ set\_spatial(self, spatial) \\ set\_temporal\_start(self, temporal\_start) \\ set\_temporal\_end(self, temporal\_end) \\ set\_issued(self, issued) \\ set\_modified(self, modified) \\ set\_provenance(self, provenance) \\ set\_conformance(self, conformance) \\ set\_contact\_name(self, contact\_name) \\ set\_contact\_email(self, contact\_email) \\ set\_distributions(self, distributions) \\ add\_distribution(self, distribution) \\ dataset\_dict(self) \\ generate\_data(self)
    			    }
    			    \umlclass[x=13,y=18]{ckan management}{
    			    }{
    			        make\_request(url, authorization\_key, data) \\ create\_dataset(ckan\_url, authorization\_key, data) \\ update\_dataset(ckan\_url, authorization\_key, data) \\ ingest\_dataset(ckan\_url, authorization\_key, data) \\ ingest\_datasets(ckan\_url, authorization\_key, datasets)
    			    }
    			    \umlclass[x=13,y=6]{OGCMetadataHarvester}{
    			        url: str \\ wms\_url: str \\ wfs\_url: str \\ wcs\_url: str
    			    }{
    			        \_\_init\_\_(self, url, wms\_url=None, wfs\_url=None, wcs\_url=None) \\ set\_url(self, url) \\ set\_wms\_url(self, wms\_url) \\ set\_wfs\_url(self, wfs\_url) \\ set\_wcs\_url(self, wcs\_url) \\ get\_wms\_url\_value(self) \\ get\_wfs\_url\_value(self) \\ get\_wcs\_url\_value(self) \\ get\_wms\_url(self) \\ get\_wfs\_url(self) \\ get\_wcs\_url(self) \\ connect\_wms(self) \\ connect\_wfs(self) \\ connect\_wcs(self) \\ get\_ckan\_name(name, owner\_org) \\ set\_min\_max\_coordinates(dataset, minx, maxx, miny, maxy) \\ set\_bounding\_box(self, dataset, bounding\_box) \\ set\_bounding\_box\_from\_bounding\_boxes(self, dataset, bounding\_boxes) \\ set\_title(dataset, title, alternate\_title) \\ set\_conformance(dataset) \\ set\_themes(dataset) \\ get\_coverage\_dataset(self, name, owner\_org, wms=None, wcs=None) \\ add\_wfs\_kewords(keywords\_array, keywords\_text) \\ get\_feature\_dataset(self, name, owner\_org, wms=None, wfs=None) \\ get\_dataset(self, name, owner\_org, wms=None, wfs=None, wcs=None) \\ get\_all\_datasets(self, owner\_org)
    			    }
    			    \umlclass[x=13,y=-8]{run}{
    			    }{
    			        ingest\_datasets(ogc\_service\_url, organization\_name, authorization\_key) \\ \_\_main\_\_()
    			    }
                \end{umlpackage}
                \umlHVHdep[arm2=8cm]{run}{ckan management}
                \umldep{run}{OGCMetadataHarvester}
                \umldep{OGCMetadataHarvester}{Dataset}
                \umldep{OGCMetadataHarvester}{Distribution}
    		\end{tikzpicture}
    	}
	\caption{Diagrama de clases pasarela OGC-CKAN}\label{fig:geockanbridge_classes}
	\end{center}
\end{figure}

El cuarto paso es la ingestión de metadatos en el servidor de Datos abiertos de la institución en cargo de la publicación de los datos de calidad del aire en el área local. Como continuación del software en el paso anterior, nuestro programa en Python transforma la información recopilada en el paso anterior a un diccionario con los objetos requeridos para construir un conjunto de datos con sus recursos asociados, que son inmediatamente insertados en un servidor local de Datos Abiertos basado en CKAN a través de su API REST\cite{ckanapi}. La columna \textit{CKAN} en la tabla \ref{tab:correspondence} indica las etiquetas que son usadas en esta estructura diccionario de datos para generar después metadatos RDF basados en GeoDCAT-AP (se ha seguido el mapeo explicado en \cite{ckanext-dcat}, sección \textit{RDF DCAT to CKAN dataset mapping}).\par
El paso final es la recolección de metadatos en los servidores locales desde los portales de Datos Abiertos regionales y nacionales hasta que al final los metadatos son últimamente recolectados por el Portal Europeo de Datos. Este paso está más allá del alcance del proyecto TRAFAIR. Sin embargo, suponemos que los portales de nivel superior se basan en la tecnología CKAN (o tienen un mecanismo similar para la recolección de catálogos suscritos de nivel inferior).

Por una parte, el plugin \textit{ckanext-dcat} de CKAN permite la publicación de metadatos de conjuntos de datos como RDF de acuerdo con los vocabularios DCAT-AP. Y por la otra parte, el complemento \textit{ckanext-harvest} de CKAN permite recoletar los contenidos de diferentes tipos de fuentes de catálogos.


\break
\section{Publicación de metadatos en Zaguan}
\label{sec:ckanzaguan}

\begin{figure}[ht]
	\centering
	\resizebox{0.65\textwidth}{!}{
		\begin{tikzpicture}
			\begin{umlpackage}{ckan zaguan}
			    \umlclass[y=0]{ckan marc}{
			    }{
			        download\_rdf(url, filename) \\ get\_zaguan\_id(identifier) \\ process\_catalog(filenames, outputfolder) \\ generate\_record(resource, graph, collection) \\ add\_datafield(record, ind1, ind2, tag) \\ add\_subfield(datafield, code, value) \\ add\_datafield\_subfield(record, ind1, ind2, tag, code, value) \\ add\_identifier(record, resource, graph) \\ add\_title(record, resource, graph) \\ add\_description(record, resource, graph) \\ add\_keyword(record, resource, graph) \\ add\_publisher(record, resource, graph) \\ add\_contact\_point(record, resource, graph) \\ add\_types(record, resource, graph) \\ add\_dcat\_types(record, resource, graph) \\ add\_spatial\_coverage(record, resource, graph) \\ add\_distribution(record, resource, graph) \\ add\_language(record, resource, graph) \\ add\_temporal\_extent(record, resource, graph) \\ add\_theme(record, resource, graph)
			    }
			    \umlclass[y=-8]{run}{
			    }{
			        insert\_into\_zaguan(file\_name) \\ \_\_main\_\_()
			    }
            \end{umlpackage}
            \umldep{run}{ckan marc}
		\end{tikzpicture}
	}
	\caption{Diagrama de clases pasarela CKAN-Zaguan}\label{fig:ckanzaguanbridge_classes}
\end{figure}

Con respecto a los repositorios de datos abiertos, actualmente la Universidad de Zaragoza cuenta con el repositorio institucional Zaguan\cite{zaguan}, montado sobre el software Invenio\cite{invenio}. Este repositorio está pensado principalmente para elementos bibliográficos y usa el estandar MARC21\cite{marc}.\par
Los conjuntos de datos de Zaguan son después regularmente volcados en Aragón Open Data\cite{aod}, de ahí al portal de datos abiertos del Gobierno de España\cite{dge} y por último al Portal Europeo de Datos\cite{edp}. Los tres portales usan principalmente el software CKAN\cite{ckan}.\par
En la figura \ref{fig:ckanzaguanbridge_classes} se puede ver las clases que componen esta pasarela. La función principal (\textit{main}) descarga un catálogo RDF desde un punto de acceso (con la función \textit{download\_rdf}), en este caso desde nuestro servidor CKAN local. Posteriormente llama a \textit{insert\_into\_zaguan}, que procesa los conjuntos de datos y series del catálogo descargado usando las funciones de la clase \textit{ckan\_marc}, que genera sus metadatos correspondientes en MARC21, y sube los datos generados a Zaguan mediante su API de ingestión.\par
En la tabla \ref{tab:correspondencezaguan} encontramos la correlación que se ha decidido implementar entre los metadatos en GeoDCAT-AP y en MARC21. Se ha intentado en todo momento emparejar cada campo con uno similar en el otro estándar, y que luego tuviese salida en la pasarela que sube los datos de Zaguan a Aragón Open Data (sobre la que desde el proyecto no tenemos control), no siendo posible para todos los metadatos presentes en el servidor CKAN local.\par
Por último, se manda un email automáticamente al administrador de Zaguan para advertir sobre la subida para que puedan tener un registro de las cargas que se hacen desde el proyecto.

\begin{table}[H]
\centering
\resizebox{0.95\textwidth}{!}{
\begin{scriptsize}
\begin{tabular}{|l|p{3.5in}|}
\hline
\textbf{GeoDCAT-AP}&\textbf{MARC21}\\
\hline
Dataset/dct:identifier&037 \#\#\$a, 970 \#\#\$a (ids \texttt{'TRAFAIR-2020-XXX'}, mapeando cada XXX al identificador GeoDCAT-AP con números ascendentes)\\
\hline
Dataset/dct:title&245 \#\#\$a\\
\hline
Dataset/dct:description&530 3\#\$a\\
\hline
Dataset/dct:type&655 \#7\$a (incluyendo en 655 \#7\$2 el valor \texttt{'inspire'})\\
\hline
Dataset/dct:language&041 \#\#\$a\\
\hline
Dataset/dcat:theme&654 \#\#\$a (incluyendo en 654 \#\#\$2 el valor \texttt{'inspire'})\\
\hline
Dataset/dcat:keyword&653 1\#\$a\\
\hline
Dataset/dct:spatial&-\\
\hline
Dataset/dct:temporal&518 \#\#\$a\\
\hline
Dataset/dct:issued&260 \#\#\$c\\
\hline
Dataset/dct:modified&-\\
\hline
Dataset/dct:provenance&-\\
\hline
Dataset/dct:conformsTo&-\\
\hline
Dataset/dct:publisher/foaf:name&260 \#\#\$b\\
\hline
Dataset/dcat:contactPoint/vcard:fn&700 \#\#\$a\\
\hline
Distribution/dcat:accessURL&856 4\#\$u\\
\hline
Distribution/dct:title&856 4\#\$n\\
\hline
Distribution/dct:format&856 4\#\$y\\
\hline
Distribution/dct:license&540 \#\#\$u, además para licencias Creative Commons: 540 \#\#\$2=\texttt{'cc'}, 540 \#\#\$a=(modificador), 540 \#\#\$b=\texttt{'Creative Commons'}, 540 \#\#\$c=(versión), 540 \#\#\$f=\texttt{'CC'} + (modificador) + (versión), 540 \#\#\$9=\texttt{'info:eu-repo/semantics/openAccess'}\\
\hline
Distribution/dct:rights&506 X\#\$u, siendo X 1 si existen limitaciones al acceso o 0 si no, y si no además: 506 0\#\$a=\texttt{'Access copy available to the general public'}, 506 0\#\$f=\texttt{'Unrestricted'}\\
\hline
Distribution/dcat:description&-\\
\hline
-&980\#\#\$X (categorías y subcategorías del portal Zaguan, siendo X ascendente alfabéticamente empezando por la a)\\
\hline
\end{tabular}
\end{scriptsize}
}
\caption{Correspondencia entre etiquetas de GeoDCAT-AP y MARC21 para la carga en Zaguan}
\label{tab:correspondencezaguan}
\end{table}

\section{Publicación de metadatos en un catálogo CSW}
\label{sec:pycsw}
Para poner en marcha un servicio de catálogo que permitiese acceder a los metadatos según el estándar ISO 19115\cite{iso19115} y que este servicio fuese compatible con la especificación OGC Catalog Services for the Web (CSW)\cite{csw} se contemplaron diversas opciones. Primero se consideró la posibilidad de instalar un complemento para catálogos CSW en Geoserver, versión que se estuvo usando en el proyecto de forma provisional durante un tiempo pero que al final fue descartada al no llegar al nivel de metadatos que requeríamos: los metadatos ISO 19115 autogenerados por GeoServer apenas informaban del título de las capas.

También se planteó el uso de GeoNetwork\cite{geonetwork}, que es un software ampliamente utilizado para la puesta en marcha de servidores y clientes de búsqueda de catálogos de metadatos geográficos. Sin embargo, tenía el inconveniente de no facilitar la sincronización directa con los metadatos que se estaban generando de forma automática según el proceso descrito en la sección \ref{sec:ckanzaguan} Geonetwork permite extraer algo de información de las capas gestionadas por Geoserver, pero el resto de elementos de metadatos se debe editar manualmente.

Finalmente, nos acabamos decantando por pyCSW\cite{pycsw}, que proporciona un servidor
de catálogo compatible con la especificación CSW desarrollado en Python, que se puede poner en marcha y gestionar de forma rápida y sencilla. Esta decisión fue reforzada al contar con una integración con CKAN a
través de ckanext-spatial\cite{ckanext-spatial}. Los metadatos GeoDCAT-AP generados con el proceso descrito en la sección 4.3 se podían reconvertir de forma automática a metadatos ISO 19115.

PyCSW cuenta con una base de datos separada a la de CKAN, a la que se hacen cargas periódicas ejecutando el comando \textit{ckan-pycsw} de ckanext-spatial.\par
En el anexo \ref{app:ckan} se incluye la instalación y el despliegue de pycsw en detalle, además del comando para la carga.


%ISO  19115  es el estándar internacional para metadatos geográficos propuesto por la Organización Internacional de Normalización (ISO por sus siglas en inglés)\cite{iso19115}, que ha sido ampliamente adoptado durante la última década en la comunidad de información geográfica en sectores tanto públicos como privados.\par
%Hay numerosos ejemplos de trabajos intentando rastrear los contenidos de los servicios OGC y automatizar la generación de elementos de metadatos que después son ingestados en catálogos que cumplen con la especificación OGC Catalog Services for the Web (CSW). Por ejemplo,  Nogueras-Iso  et  al.  \cite{Nogueras2009} propusieron un mecanismo para derivar metadatos desde la información Capabilities devuelta por los servicios OGC (WMS, WFS o WCS) y crear entradas en un catálogo de servicios de información geográfica.\par
